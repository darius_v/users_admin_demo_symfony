How to install and run:

Create database. Update database settings in app/config/parameters.yml

`composer install`

`php bin/console doctrine:migrations:migrate`

`php bin/console server:start`

For each request, do not forget to pass

`X-AUTH-TOKEN`

 header with value:

`secret api key`


Go to http://localhost:8000

For more urls - see API documentation.

To run behat tests, run in linux shell:

`vendor/bin/behat`

PHP server must be running in order to pass all tests.

Note: Some errors defined in API documentation are not implemented.

