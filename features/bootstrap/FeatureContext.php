<?php

use AppBundle\Repository\GroupRepositoryInterface;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\UserRepositoryInterface;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use AppBundle\Entity\User;
use AppBundle\Entity\Group;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * // TODO could move each feature to seperate context file.
 * // TODO move to tests folder
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    private $userRepository;

    private $user;
    private $group;

    private $browserClient;
    private $session;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        $baseUrl,
        GroupRepositoryInterface $groupRepository
    ){
        $this->userRepository = $userRepository;
        $this->groupRepository = $groupRepository;
        $this->baseUrl = $baseUrl;

        // Choose a Mink driver. More about it in later chapters.
        $driver = new \Behat\Mink\Driver\GoutteDriver();

        $this->session = new \Behat\Mink\Session($driver);

        // start the session
        $this->session->start();

        $this->session->setRequestHeader('Content-Type', 'application/json');

        $this->browserClient = $this->session->getDriver()->getClient();

        $this->browserClient->setHeader('X-AUTH-TOKEN', 'secret api key');
    }

    /**
     * @Given There is no user :arg1
     */
    public function thereIsNoUser($arg1)
    {
        $user = $this->userRepository->findOneBy(['name' => $arg1]);
        if ($user) {
            $this->userRepository->delete($user);
        }
    }

    /**
     * @When I add the user with name :arg1
     */
    public function iAddTheUserWithName($arg1)
    {
        $user = new User($arg1);
        $this->userRepository->add($user);
    }

    /**
     * @Then I should have user :arg1 in a system
     */
    public function iShouldHaveUserInASystem($arg1)
    {
        $user = $this->userRepository->findOneBy(['name' => $arg1]);

        if (!$user) {
            throw new Exception('User was not added');
        }

        $this->userRepository->delete($user);
    }

    /**
     * @Given There is user
     */
    public function thereIsUser()
    {
        $this->createTestUser();
    }

    /**
     * @When I delete the user
     */
    public function iDeleteTheUser()
    {
        $this->browserClient->request('DELETE', $this->baseUrl . '/user/' . $this->user->getId());
    }

    /**
     * @Then There should not be user in the system
     */
    public function thereShouldNotBeUserInTheSystem()
    {
        $data = $this->session->getPage();

        $data = json_decode($data->getContent(), true);

        if (!isset($data['data']) || $data['data'] != 'Deleted succesfully') {
            throw new Exception('After calling delete user, did not get correct message');
        }
    }

    /**
     * @Given There is a group
     */
    public function thereIsAGroup()
    {
        $this->group = $this->groupRepository->findOneBy([]);

        if (!$this->group) {
            $this->group = new Group('test group');
            $this->groupRepository->add($this->group);
        }
    }

    private function createTestUser()
    {
        $this->user = new User('Test');
        $this->userRepository->add($this->user);
    }

    /**
     * @Given User is not assigned to a group
     */
    public function userIsNotAssignedToAGroup()
    {
        $this->createTestUser();
    }

    /**
     * @When I assign user to a group
     */
    public function iAssignUserToAGroup()
    {
        $this->browserClient->request(
            'PUT',
            $this->baseUrl . '/user/' . $this->user->getId() . '/group/' . $this->group->getId()
        );
    }

    /**
     * @Then User has to be assigned to a group
     */
    public function userHasToBeAssignedToAGroup()
    {
        $data = $this->session->getPage();

        $data = json_decode($data->getContent(), true);

        if (!isset($data['user_id']) || !isset($data['group_id'])) {
            throw new Exception('After assigning user to a group, there is no user id or group id');
        }
    }

    /**
     * @Given There is user assigned to a group
     */
    public function thereIsUserAssignedToAGroup()
    {
        // the user and group is not here anymore, so create again and assign.
        $this->createTestUser();
        $this->thereIsAGroup();
        $this->iAssignUserToAGroup();
    }

    /**
     * @When I remove user from a group
     */
    public function iRemoveUserFromAGroup()
    {
        $this->browserClient->request(
            'DELETE',
            $this->baseUrl . '/user/' . $this->user->getId() . '/group/' . $this->group->getId()
        );
    }

    /**
     * @Then User has to be removed from a group
     */
    public function userHasToBeRemovedFromAGroup()
    {
        throw new PendingException();
    }
}
