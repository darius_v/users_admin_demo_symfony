Feature: Assign user to a group, remove from group
  In order to have assigned user to a group
  As an admin
  I need to be able to assign user to a group

  Rules:
  - User can be assigned to a group if is not yet assigned

  Scenario: Assigning user to a group
    Given There is user
    And There is a group
    And User is not assigned to a group
    When I assign user to a group
    Then User has to be assigned to a group

  Scenario: Removing user from a group
    Given There is user assigned to a group
    When I remove user from a group
    Then User has to be removed from a group
