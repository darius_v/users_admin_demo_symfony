Feature: Add users
  In order to have users
  As an admin
  I need to be able to add users to database

  Rules:
  - User has a name

  Scenario: Adding user with name 'Jonas'
    Given There is no user 'Jonas'
    When I add the user with name 'Jonas'
    Then I should have user 'Jonas' in a system
