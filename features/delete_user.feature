Feature: Delete user
  In order to not have useless users
  As an admin
  I need to be able to delete users from system

  Rules:
  - User to be delede is identified by id

  Scenario: Deleting user
    Given There is user
    When I delete the user
    Then There should not be user in the system

