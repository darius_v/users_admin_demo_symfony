<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Group;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GroupController extends Controller
{
    /**
     * TODO: 409 response
     *
     * @Route("/group")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function createGroup(Request $request)
    {
        $groupUserService = $this->container->get('group_user_service');

        $content = json_decode($request->getContent(), true);

        $group = new Group($content['name']);

        $group = $groupUserService->createGroup($group, true);

        $this->getDoctrine()->getEntityManager()->flush();

        $response = new JsonResponse();
        $response->setStatusCode(201);
        $response->setData(array(
            'links' => [
                'delete' => $this->generateUrl('delete_group', ['id' => $group->getId()])
            ],
            'id'   => $group->getId(),
            'name' => $group->getName()
        ));

        return $response;
    }

    /**
     * TODO: validation error code - when not found such group by id - will throw error - object not found
     * @Route("/group/{id}", name="delete_group")
     * @Method("DELETE")
     * @param Group $group
     * @return JsonResponse
     */
    public function delete(Group $group)
    {
        $groupUserService = $this->container->get('group_user_service');

        $deleted = $groupUserService->deleteGroupIfHasNoUsers($group);

        $response = new JsonResponse();

        if ($deleted) {
            $response->setData(array(
                'data' => 'Deleted succesfully'
            ));

        } else {

            $response->setStatusCode(405);

            $response->setData(array(
                'data' => 'First need to delete users from group'
            ));
        }

        return $response;
    }
}
