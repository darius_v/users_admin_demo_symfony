<?php

namespace AppBundle\Controller;

use AppBundle\Entity\GroupUser;
use AppBundle\Entity\User;
use AppBundle\Entity\Group;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

class GroupUserController extends Controller
{
    /**
     * TODO: rest status code on failed insert when duplicate
     * @Route("/user/{id}/group/{group_id}"), name="assign_user_to_group"
     * @ParamConverter("group", class="AppBundle:Group", options={"mapping": {"group_id": "id"}})
     * @Method("PUT")
     * @param User $user
     * @param Group $group
     * @return JsonResponse
     */
    public function assignUserToGroup(User $user, Group $group)
    {
        $groupUserRepo = $this->container->get('group_user_repository');

        $groupUser = new GroupUser($user, $group);

        $groupUserRepo->add($groupUser);

        $response = new JsonResponse();
        $response->setStatusCode(201);
        $response->setData(array(
            'links' => [
                'delete' => $this->generateUrl('remove_user_from_group', ['id' => $user->getId(), 'group_id' => $group->getId()])
            ],
            'user_id' => $user->getId(),
            'group_id' => $group->getId()
        ));

        return $response;
    }

    /**
     * TODO: inform that data did not exist which we tried to delete and status code
     *
     * TODO: maybe in responses use message instead of data everywhere?
     * @Route("/user/{id}/group/{group_id}", name="remove_user_from_group")
     * @ParamConverter("group", class="AppBundle:Group", options={"mapping": {"group_id": "id"}})
     * @Method("DELETE")
     * @param User $user
     * @param Group $group
     * @return JsonResponse
     */
    public function removeUserFromGroup(User $user, Group $group)
    {
        $groupUserRepository = $this->container->get('group_user_repository');

        $groupUserRepository->delete($user, $group);

        $response = new JsonResponse();
        $response->setData(array(
            'data' => 'Deleted succesfully'
        ));

        return $response;
    }
}
