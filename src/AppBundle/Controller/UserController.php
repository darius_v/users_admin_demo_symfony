<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\User;

// TODO refactor
class UserController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return JsonResponse
     * @throws \Exception
     */
    public function indexAction()
    {
        $response = new JsonResponse();
        $response->setData(array(
            'data' => 'Users admin demo home'
        ));

        return $response;
    }

    /**
     * TODO: refactor, maybe require unique user name?
     * TODO: 409 response
     * TODO: add delete link in response
     * @Route("/user"), name="user"
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createUser(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        $name = $content['name'];

        $userRepository = $this->container->get('user_repository');

        $user = new User($name);

        $userRepository->add($user);

        $response = new JsonResponse();
        $response->setStatusCode(201);
        $response->setData(array(
            'links' => [
                'delete' => $this->generateUrl('delete_user', ['id' => $user->getId()])
            ],
            'id' => $user->getId(),
            'name' => $name
        ));

        return $response;
    }

    /**
     * TODO: message when does not exits
     * @Route("/user/{id}", name="delete_user")
     * @Method("DELETE")
     * @param User $user
     * @return JsonResponse
     */
    public function delete(User $user)
    {
        $userRepository = $this->container->get('user_repository');

        $userRepository->delete($user);

        $response = new JsonResponse();
        $response->setData(array(
            'data' => 'Deleted succesfully'
        ));

        return $response;
    }
}
