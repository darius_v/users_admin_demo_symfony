<?php

namespace AppBundle\Service;

use AppBundle\Repository\GroupEntityRepositoryInterface;
use AppBundle\Repository\GroupRepositoryInterface;
use AppBundle\Repository\GroupUserRepositoryInterface;
use AppBundle\Entity\Group;

class GroupUserService
{
    private $groupRepository;
    private $groupUserRepository;

    /**
     * @param GroupRepositoryInterface $groupRepository
     * @param GroupUserRepositoryInterface $groupUserRepository
     */
    public function __construct(GroupRepositoryInterface $groupRepository, GroupUserRepositoryInterface $groupUserRepository)
    {
        $this->groupRepository = $groupRepository;
        $this->groupUserRepository = $groupUserRepository;
    }

    /**
     * @param Group $group
     * @return boolean - true on delete
     */
    public function deleteGroupIfHasNoUsers(Group $group)
    {
        $groupUser = $this->groupUserRepository->findOneBy(
            ['group' => $group]
        );

        if ($groupUser === null) {

            $this->groupRepository->delete($group);
            return true;
        }

        return false;
    }

    /**
     * TODO: when group name is not unique, give error
     * @param Group $group
     * @return Group
     */
    public function createGroup(Group $group)
    {
        $this->groupRepository->add($group, true);

        return $group;
    }
}
