<?php

namespace AppBundle\Repository;

use AppBundle\Entity\GroupUser;
use AppBundle\Entity\User;
use AppBundle\Entity\Group;
use Doctrine\ORM\EntityRepository;

class GroupUserRepository extends EntityRepository implements GroupUserRepositoryInterface
{
    /**
     * @param GroupUser $groupUser
     */
    public function add(GroupUser $groupUser)
    {
        $this->getEntityManager()->persist($groupUser);
        $this->getEntityManager()->flush();
    }

    /**
     * @param User $user
     * @param Group $group
     */
    public function delete(User $user, Group $group)
    {
        $groupUser = $this->findOneBy(
            [
                'user' => $user,
                'group' => $group
            ]
        );

        $this->getEntityManager()->remove($groupUser);
        $this->getEntityManager()->flush();
    }
}
