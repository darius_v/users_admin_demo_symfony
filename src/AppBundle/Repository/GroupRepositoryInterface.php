<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Group;

interface GroupRepositoryInterface
{
    /**
     * @param Group $group
     */
    public function add(Group $group);

    /**
     * @param Group $group
     */
    public function delete(Group $group);
}
