<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;

interface UserRepositoryInterface
{
    public function add(User $user);

    public function delete(User $user);
}
